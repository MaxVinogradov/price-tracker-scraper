import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello')

channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=str(json.loads(open('data_old.txt').read())))
print(" [x] Sent 'Hello World!'")
connection.close()
