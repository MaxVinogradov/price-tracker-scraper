from scrapy import Field, Item


class ProductItem(Item):
    name = Field()
    category = Field()
    link = Field()
    price = Field()
    date = Field()
