BOT_NAME = 'tutorial'

SPIDER_MODULES = ['tutorial.spiders']
NEWSPIDER_MODULE = 'tutorial.spiders'
LOG_LEVEL = 'INFO'
REDIRECT_MAX_TIMES = 10

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:39.0) Gecko/20100101 Firefox/39.0'

ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 32

ITEM_PIPELINES = {
   'tutorial.pipelines.ProductPipeline': 300,
}


FEED_EXPORT_ENCODING = 'utf-8'
