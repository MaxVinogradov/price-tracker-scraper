import scrapy
import json
from datetime import datetime
import tutorial.db as db


class ProductPipeline(object):
    def __init__(self):
        self.items = []

    @classmethod
    def from_crawler(cls, crawler):
        temp = cls()
        crawler.signals.connect(temp.spider_closed, signal=scrapy.signals.spider_closed)
        return temp

    def process_item(self, item, spider):
        print('product_name= {}'.format(item))
        item['date'] = datetime.now()
        # db.save_product_item(dict(item))
        self.items.append(dict(item))

    def spider_closed(self, spider):
        print("Finish@@@")
        print(len(self.items))
        db.save_product_all_items(list(self.items))
        # with open('data2.txt', 'w') as outfile:
        #     json.dump(self.items, outfile, ensure_ascii=False, indent=4)
