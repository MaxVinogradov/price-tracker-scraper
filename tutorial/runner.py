from subprocess import call
import json
import redis
import logging
import itertools
import common

def cleanup_redis_structure(configuration):
    r = redis.StrictRedis(
        host=configuration['host'],
        port=configuration['port'],
        db=configuration['db']
    )
    r.delete('GOODS')


def build_init_redis_structure(configuration):
    r = redis.StrictRedis(
        host=configuration['host'],
        port=configuration['port'],
        db=configuration['db']
    )
    # r.hset(name='GOODS', key='test', value='')


if __name__ == "__main__":
    print(common.R)
    # config = json.loads(open('config/config.json').read())
    # if config['cleanup_structure']:
    #     cleanup_redis_structure(config)
    # if config['is_first_launch']:
    #     logging.log(logging.INFO, 'INIT LAUNCH')
    #     build_init_redis_structure(config)
    # call('scrapy crawl comfy')
    # call('scrapy crawl foxtrot')
    # call('scrapy crawl moyo')
    # call('scrapy crawl rozetka')
