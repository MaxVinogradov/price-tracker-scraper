from scrapy.spiders import CrawlSpider
from tutorial.spiders.common_spider import SpiderTools


class MoyoSpider(CrawlSpider, SpiderTools):
    CONFIG, name, allowed_domains, start_urls, rules = SpiderTools.init_spider_data('moyo-.json')

    def parse_products(self, response):
        for item in SpiderTools.get_items(response, MoyoSpider.CONFIG):
            yield item

