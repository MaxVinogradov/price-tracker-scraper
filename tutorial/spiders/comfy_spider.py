from scrapy.spiders import CrawlSpider

from tutorial.spiders.common_spider import SpiderTools


class ComfySpider(CrawlSpider, SpiderTools):
    CONFIG, name, allowed_domains, start_urls, rules = SpiderTools.init_spider_data('comfy.json')

    def parse_products(self, response):
        for item in SpiderTools.get_items(response, ComfySpider.CONFIG):
            yield item
