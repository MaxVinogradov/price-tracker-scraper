import re
import os
import json
from typing import Dict, List, Tuple
from scrapy.http import Response
from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor
from tutorial.items import ProductItem


class SpiderTools:
    CONFIG_FOLDER = 'tutorial{}config{}data{}'.format(*[os.sep] * 3)
    MAIN_RULE_CALLBACK = 'parse_products'

    @staticmethod
    def generate_rule(rule: str):
        return (
            Rule(LinkExtractor(restrict_xpaths=rule), callback=SpiderTools.MAIN_RULE_CALLBACK, follow=True),
        )

    @staticmethod
    def init_spider_data(config_file: str) -> Tuple:
        cfg = json.load(open(SpiderTools.CONFIG_FOLDER + config_file))
        return cfg, cfg['name'], cfg['domains'], cfg['start_urls'], SpiderTools.generate_rule(cfg['rule'])

    @staticmethod
    def get_data_by_xpath(response: Response, xpath: str, index: int = 0) -> str:
        return str(response.xpath(xpath).extract()[index].encode('utf8').strip(), 'utf-8')

    @staticmethod
    def get_items(response: Response, config: Dict) -> List[ProductItem]:
        result = []
        data = config['xpathes']
        category = SpiderTools.get_data_by_xpath(response, data['category'])
        for index, product_item in enumerate(response.xpath(data['product_items'])):
            item = ProductItem()
            item['name'] = SpiderTools.get_data_by_xpath(response, data['name'], index)
            item['category'] = category
            item['link'] = SpiderTools.get_data_by_xpath(response, data['link'], index)
            item['price'] = re.sub("[^0-9.,]", "", SpiderTools.get_data_by_xpath(response, data['price'], index))
            result.append(item)
        return result
