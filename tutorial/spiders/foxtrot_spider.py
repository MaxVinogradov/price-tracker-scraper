from scrapy.spiders import CrawlSpider

from tutorial.spiders.common_spider import SpiderTools


class FoxtrotSpider(CrawlSpider, SpiderTools):
    CONFIG, name, allowed_domains, start_urls, rules = SpiderTools.init_spider_data('foxtrot.json')

    def parse_products(self, response):
        for item in SpiderTools.get_items(response, FoxtrotSpider.CONFIG):
            yield item