import json

from scrapy.spiders import CrawlSpider
from scrapy.http import Request

from tutorial.spiders.common_spider import SpiderTools
from tutorial.items import ProductItem


class RozetkaSpyder(CrawlSpider, SpiderTools):
    CONFIG, name, allowed_domains, start_urls, rules = SpiderTools.init_spider_data('rozetka.json')

    def parse_products(self, response):
        data = RozetkaSpyder.CONFIG['xpathes']
        category = SpiderTools.get_data_by_xpath(response, data['category'])
        for index, product_item in enumerate(data['product_items']):
            item = ProductItem()
            item['name'] = SpiderTools.get_data_by_xpath(response, data['name'], index)
            item['category'] = category
            item['link'] = SpiderTools.get_data_by_xpath(response, data['link'], index)
            item = Request(item['link'], meta={'item': item}, callback=self.parse_price)
            yield item

    def parse_price(self, response):
        item = response.meta['item']
        data = response.selector.xpath('//script').re(r'dataLayer.push\(\{\"pageType\".*')[0]
        item['price'] = json.loads(data.replace(");</script>", "").replace("dataLayer.push(", ""))['productPriceLocal']
        return item

