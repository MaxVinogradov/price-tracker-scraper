from pymongo import MongoClient
from datetime import datetime
import tutorial.common as common

common.init_config()
mongodb = common.CONFIG['mongodb']
client = MongoClient(mongodb['host'], mongodb['port'])
db = client[mongodb['db']]
productsCollection = db['scraped-products']


def save_product_item(product):
    productsCollection.insert_one(product)


def save_product_all_items(product_list=[]):
    productsCollection.insert(product_list)
