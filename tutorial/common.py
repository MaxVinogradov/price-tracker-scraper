import logging
import json
import sys
from datetime import datetime

LOG = None
CONFIG = None
R = 11


def init_logger():
    global LOG
    LOG = logging.getLogger('SHOP_SPIDER')
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)

    fh = logging.FileHandler("'logs/spiders/log_{}.log".format(datetime.now()))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    LOG.addHandler(ch)
    LOG.addHandler(fh)
    return LOG


def init_config():
    global CONFIG
    CONFIG = json.loads(open(r'C:\Users\LENOVO\Desktop\desktop\diplom\scrapShopSpider\tutorial\tutorial\config\config.json').read())

